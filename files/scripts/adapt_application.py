import yaml
import sys

image = sys.argv[1]

fn_src = "application.yml.in"
fn_out = "application.yml"
fn_users = "users.yml"

with open(fn_users, 'r') as file:
    users = yaml.safe_load(file)


with open(fn_src, 'r') as file:
    app = yaml.safe_load(file)

app['proxy']['users']=users

for n in range(0,len(app['proxy']['specs'])):
    if app['proxy']['specs'][n]['id']=='shinyscreen':
        app['proxy']['specs'][n]['container-image']=image
        break

with open(fn_out, 'w') as file:
    yaml.dump(app, file, sort_keys=False, default_flow_style=False)

