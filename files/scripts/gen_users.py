import yaml
import sys
import random, string


# Take one argument, the `group file'.
fn_groups = sys.argv[1]

with open(fn_groups, 'r') as file:
    groups = yaml.safe_load(file)

# The group file is in yaml format:
# group1: no_members_1
# group2: no_members_2
# ... 


def gen_members(group,no):
    return([group+str(k) for k in range(1,no+1)])
# Define a function that takes a group name and generates `no_members'
# members.

def gen_pass():
    chrs = string.ascii_lowercase
    return(''.join(random.choice(chrs) for i in range(6)))


def user_entries(group,no):
    members=gen_members(group,no)
    entries = [ {"name":u,"groups":group,"password":gen_pass()} for u in members ]
    return(entries)
# Define a function that returns an n-digit random string.

# Populate users dictionary with entries of this format:
# user: group_1
# group: group1
# password: randomly generated password

res = []

for group in groups.keys():
    res = res + user_entries(group,int(groups[group]))


with open("users.yaml", 'w') as file:
    yaml.dump(res, file, sort_keys=False, default_flow_style=False)
